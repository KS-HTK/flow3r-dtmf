#flow3r-dtmf

A simple DTMF tone generator for the CCCamp2023 badge.

Petals are numbers (see PCB silkscreen)
App Controller is used for star (left) and pound (right). Press will play the tone sequence.

vol_up will also remove last input.

## Transferring the app over REPL

```shell
mpremote mkdir :/flash/sys/apps/dtmf
mpremote cp ./dtmf/* :/flash/sys/apps/KS-HTK-flow3r-dtmf/
```

Hz  |1209|1336|1477|
----|----|----|----|
697 |  1 |  2 |  3 |
770 |  4 |  5 |  6 |
852 |  7 |  8 |  9 |
941 |  * |  0 |  # |


----
### Unused

#### Normalized to equal loudness (dB) for the Headphone output

lowpass settings:
freq|mult|
----|----|
697 | .1 |
770 | .2 |
852 | 15 |
941 | .5 |
1209| .3 |
1336| .3 |
1477| .2 |

replaced 852 with 857 to reduce multiplication to .5