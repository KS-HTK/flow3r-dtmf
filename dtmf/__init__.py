from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.ui.view import BaseView, ViewManager
from st3m.ui.colours import GO_GREEN
from ctx import Context
from sys_kernel import freertos_sleep as sleep

import audio
import leds
import st3m.run
import bl00mbox


FONT_SIZE: int = 35
FONT: int = 5
#857 is a hack to not use a 15 multiplicaiton in the lowpass
FREQ = [697, 770, 857, 941, 1209, 1336, 1477, 1633]
LPF = [1,1,1,1,1,1,1,1]
# unused (normalized for headphone output)
#LPF = [.1, .2, .5, .5, .3, .3, .2, .5]

LBTN = '*'
RBTN = '#'

PETAL_LEDS = {
  0: (38, 43),
  1: (2, 7),
  2: (6, 11),
  3: (10, 15),
  4: (14, 19),
  5: (18, 23),
  6: (22, 27),
  7: (26, 31),
  8: (30, 35),
  9: (34, 39),
}

PETAL_COLORS = {
  0: (1.0, 0.0, 0.0),
  1: (1.0, 0.6, 0.0),
  2: (0.8, 1.0, 0.0),
  3: (0.2, 1.0, 0.0),
  4: (0.0, 1.0, 0.4),
  5: (0.0, 1.0, 1.0),
  6: (0.0, 0.4, 1.0),
  7: (0.2, 0.0, 1.0),
  8: (0.8, 0.0, 1.0),
  9: (1.0, 0.0, 0.6),
}

class DTMF(Application):
  def __init__(self, app_ctx: ApplicationContext) -> None:
    super().__init__(app_ctx)
    self._num_str: str = '' #stores input
    self.blm: Channel = bl00mbox.Channel()
    self.dtmf_codes = {} #stores tone generators
    self._setup()

  def _setup(self) -> None:
    # --- Set global leds brightness ---
    leds.set_brightness(69)
    # --- Sound generation stuff ---
    self.blm.volume = 15000

    # generate tone oscillators and lowpass filters
    lpfs = []
    for freq, lpf in zip(FREQ, LPF):
      osc = self.blm.new(bl00mbox.plugins.osc_fm)
      osc.signals.waveform = 0
      osc.signals.pitch.freq = freq
      lp = self.blm.new(bl00mbox.plugins.lowpass)
      lp.signals.freq = freq
      lp.signals.gain.mult = lpf
      lp.signals.input = osc.signals.output
      lpfs.append(lp)

    # --- dtmf stuff ---
    self.dtmf_codes = {
      '1': [lpfs[0], lpfs[4]],
      '2': [lpfs[0], lpfs[5]],
      '3': [lpfs[0], lpfs[6]],
      '4': [lpfs[1], lpfs[4]],
      '5': [lpfs[1], lpfs[5]],
      '6': [lpfs[1], lpfs[6]],
      '7': [lpfs[2], lpfs[4]],
      '8': [lpfs[2], lpfs[5]],
      '9': [lpfs[2], lpfs[6]],
      '*': [lpfs[3], lpfs[4]],
      '0': [lpfs[3], lpfs[5]],
      '#': [lpfs[3], lpfs[6]], 
    }

  def draw(self, ctx: Context) -> None:
    # configure font and text position
    ctx.font_size = FONT_SIZE
    ctx.font = ctx.get_font_name(FONT)
    ctx.text_baseline = ctx.MIDDLE
    ctx.text_align = ctx.CENTER

    # Paint the background black
    ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
    ctx.rgb(*GO_GREEN)

    # Paint the text in white
    ctx.move_to(0, 0)
    ctx.save()
    ctx.scale(1.0, 1)
    ctx.text(self._num_str)
    ctx.restore()
  
  def think(self, ins: InputState, delta_ms: int) -> None:
    super().think(ins, delta_ms) # Let Application do its thing
    
    left_btn = self.input.buttons.app

    # if petal pressed add to input string
    for i in range(10):
      petal = self.input.captouch.petals[i].whole
      if petal.down:
        ptl_press = True
      if petal.pressed:
        ptl_press = True
        self._num_str += str(i)
        self._light_petal(i, True)
        self._play_tone(str(i))
      if petal.released:
        self._light_petal(i)
        self._stop_tone(str(i))
    
    # check left button states
    if left_btn.left.pressed:
      self._num_str += LBTN
      self._play_tone(LBTN)
    if left_btn.left.released:
      self._stop_tone(LBTN)
    if left_btn.right.pressed:
      self._num_str += RBTN
      self._play_tone(RBTN)
    if left_btn.right.released:
      self._stop_tone(RBTN)
    if left_btn.middle.pressed:
      self._dial(delta_ms)
      self._num_str = ''
    
    # check for backspace
    if self.input.buttons.os.right.pressed:
      self._num_str = self._num_str[:-1]

  # function to dial a number
  def _dial(self, delta_ms: int) -> None:
    print("[DTMF] Dialing: " + self._num_str)
    audio_vol = audio.speaker_get_volume_dB()
    # set system volume
    audio_max = audio.speaker_get_maximum_volume_dB()
    audio.speaker_set_volume_dB(audio_max//2)
    
    # iterate through input and play tones
    # uses sleep to get timing right
    for c in self._num_str:
      self._play_tone(c)
      sleep(100)

      self._stop_tone(c)
      sleep(50)

    audio.speaker_set_volume_dB(audio_vol)

  def _play_tone(self, tone: str) -> None:
    lo, hi = self.dtmf_codes[tone]
    lo.signals.output = self.blm.mixer
    hi.signals.output = self.blm.mixer
  def _stop_tone(self, tone: str) -> None:
    lo, hi = self.dtmf_codes[tone]
    lo.signals.output = None
    hi.signals.output = None

  def _light_petal(self, petal: int, switch_on=False) -> None:
    color: (float, float, float) = (0, 0, 0) # 0-1 
    brightness: int = 0 # 0-255
    if switch_on:
      color = PETAL_COLORS[petal]
    for i in range(*PETAL_LEDS[petal]):
      leds.set_rgb(i%40, *color)
    leds.update()

if __name__ == '__main__':
  st3m.run.run_view(DTMF(ApplicationContext()))